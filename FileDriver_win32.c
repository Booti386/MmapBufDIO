/**
 * \file
 * \brief Windows-compatible IFileDriver implementation
 */

#include "platform.h"

#include <stdlib.h>
#include <windows.h>

#include "types.h"
#include "error.h"
#include "IFileDriver.h"
#include "FileDriver_win32.h"

/**
 * \class IDriverFileImplWin32
 * \implements IDriverFile
 */
struct IDriverFileImplWin32 {
	struct IDriverFile iface;

	struct IDriverFileImplWin32 *prev, *next;
	DWORD alloc_gran;
	IoMode io_mode;
	DWORD map_prot;
	DWORD map_access;
	HANDLE file_handle;
	HANDLE map_handle;
	uint64_t map_len;
	struct map_entry {
		struct map_entry *prev, *next;
		void *real_ptr;
		uint64_t real_len;
		void *ptr;
	} *mappings;
};

/**
 * \class IFileDriverImplWin32
 * \implements IFileDriver
 */
struct IFileDriverImplWin32 {
	struct IFileDriver iface;

	Boolean inited;
	DWORD alloc_gran;
	struct IDriverFileImplWin32 *files;
};

static struct IDriverFileVtbl DriverFileVtbl;
static struct IFileDriverVtbl FileDriverVtbl;
static struct IFileDriverImplWin32 FileDriverImpl;

/******* IDriverFile functions *******/

static inline struct IDriverFileImplWin32 *impl_from_IDriverFile(struct IDriverFile *iface) {
	return CONTAINING_RECORD(iface, struct IDriverFileImplWin32, iface);
}

static generic_error IDriverFile_Close(struct IDriverFile *iface)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	while(This->mappings)
		DriverFileVtbl.Munmap(iface, This->mappings->ptr);
	if(This->map_handle)
		CloseHandle(This->map_handle);
	CloseHandle(This->file_handle);
	if(This->next)
		This->next->prev = This->prev;
	if(This->prev)
		This->prev->next = This->next;
	else /* If This is the first entry. */
		FileDriverImpl.files = This->next;
	free(This);
	return SUCCESS;
}

static generic_error IDriverFile_GetSize(struct IDriverFile *iface, uint64_t *dst_size)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	DWORD low, high;

	if(!iface || !dst_size)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	low = GetFileSize(This->file_handle, &high);
	if(low == INVALID_FILE_SIZE) {
		DWORD err = GetLastError();
		switch(err) {
			case NO_ERROR: break;
			default:       return -ERR_UNK;
		}
	}

	*dst_size = (uint64_t)low | ((uint64_t)high << 32);
	return SUCCESS;
}

static generic_error IDriverFile_Mmap(struct IDriverFile *iface, uint64_t off, uint64_t len, void **map_ptr)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	uint64_t page_idx;
	void *ptr;
	struct map_entry *mapping;

	if(!iface || !len || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	if(This->map_handle
			&& off + len > This->map_len) {
		CloseHandle(This->map_handle);
		This->map_handle = NULL;
	}

	if(!This->map_handle) {
		uint64_t map_len = off + len;
		This->map_handle = CreateFileMapping(This->file_handle,
				NULL,
				This->map_prot,
				(DWORD)(map_len << 32),
				(DWORD)map_len,
				NULL
			);
		if(!This->map_handle)
			return -ERR_NOMEM;
		This->map_len = map_len;
	}

	page_idx = off / This->alloc_gran;
	off -= page_idx * This->alloc_gran;
	ptr = MapViewOfFile(This->map_handle, This->map_access, (DWORD)(page_idx >> 32), (DWORD)page_idx, len + off);
	if(!ptr)
		return -ERR_NOMEM;
	mapping = malloc(sizeof(*mapping));
	if(!mapping) {
		UnmapViewOfFile(ptr);
		return -ERR_NOMEM;
	}
	if(!This->mappings) {
		This->mappings = mapping;
		mapping->prev = NULL;
		mapping->next = NULL;
	} else {
		mapping->prev = NULL;
		mapping->next = This->mappings;
		This->mappings->prev = mapping;
		This->mappings = mapping;
	}
	mapping->real_ptr = ptr;
	mapping->real_len = len + off;
	mapping->ptr = (void *)((intptr_t)ptr + (intptr_t)off);
	*map_ptr = mapping->ptr;
	return SUCCESS;
}

static generic_error IDriverFile_Mflush(struct IDriverFile *iface, void *map_ptr)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	struct map_entry *mapping;
	BOOL bret;

	if(!iface || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	mapping = This->mappings;
	while(mapping) {
		if(mapping->ptr == map_ptr)
			break;
		mapping = mapping->next;
	}
	if(!mapping)
		return -ERR_INVL;

	bret = FlushViewOfFile(mapping->real_ptr, mapping->real_len);
	if(!bret) {
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IDriverFile_Munmap(struct IDriverFile *iface, void *map_ptr)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	struct map_entry *mapping;
	BOOL bret;

	if(!iface || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	mapping = This->mappings;
	while(mapping) {
		if(mapping->ptr == map_ptr)
			break;
		mapping = mapping->next;
	}
	if(!mapping)
		return -ERR_INVL;

	bret = UnmapViewOfFile(mapping->real_ptr);
	if(!bret) {
		return -ERR_UNK;
	}

	if(mapping->next)
		mapping->next->prev = mapping->prev;
	if(mapping->prev)
		mapping->prev->next = mapping->next;
	else /* If mapping is the first entry. */
		This->mappings = mapping->next;
	free(mapping);
	return SUCCESS;
}

static generic_error IDriverFile_Read(struct IDriverFile *iface, uint64_t off, uint64_t len, void *dst_ptr)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	LARGE_INTEGER ioff;
	LARGE_INTEGER ret_off;
	DWORD size_read;
	BOOL bret;

	if(!iface || !len || !dst_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & read_mode))
		return -ERR_INVL;

	ioff.QuadPart = (int64_t)off;
	bret = SetFilePointerEx(This->file_handle, ioff, &ret_off, FILE_BEGIN);
	if(!bret)
		return -ERR_UNK;
	if(ret_off.QuadPart != (int64_t)off)
		return -ERR_UNK;

	while(len > 0) {
		bret = ReadFile(This->file_handle, dst_ptr, len, &size_read, NULL);
		if(!bret)
			return -ERR_IO;
		if(size_read == 0)
			return -ERR_EOF;
		if((uint64_t)size_read > len)
			break; /* Not sure... */
		len -= size_read;
		dst_ptr = (void *)((intptr_t)dst_ptr + size_read);
	}
	return SUCCESS;
}

static generic_error IDriverFile_Write(struct IDriverFile *iface, uint64_t off, uint64_t len, void *src_ptr)
{
	struct IDriverFileImplWin32 *This = impl_from_IDriverFile(iface);
	LARGE_INTEGER ioff;
	LARGE_INTEGER ret_off;
	DWORD size_written;
	BOOL bret;

	if(!iface || !len || !src_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & write_mode))
		return -ERR_INVL;

	ioff.QuadPart = (int64_t)off;
	bret = SetFilePointerEx(This->file_handle, ioff, &ret_off, FILE_BEGIN);
	if(!bret)
		return -ERR_UNK;
	if(ret_off.QuadPart != (int64_t)off)
		return -ERR_UNK;

	while(len > 0) {
		bret = WriteFile(This->file_handle, src_ptr, len, &size_written, NULL);
		if(!bret)
			return -ERR_IO;
		if(size_written == 0)
			return -ERR_UNK;
		if((uint64_t)size_written > len)
			break; /* Not sure... */
		len -= size_written;
		src_ptr = (void *)((intptr_t)src_ptr + size_written);
	}
	return SUCCESS;
}

static struct IDriverFileVtbl DriverFileVtbl = {
	IDriverFile_Close,
	IDriverFile_GetSize,
	IDriverFile_Mmap,
	IDriverFile_Mflush,
	IDriverFile_Munmap,
	IDriverFile_Read,
	IDriverFile_Write
};


/******* IFileDriver functions *******/

static inline struct IFileDriverImplWin32 *impl_from_IFileDriver(struct IFileDriver *iface) {
	return CONTAINING_RECORD(iface, struct IFileDriverImplWin32, iface);
}

static generic_error FileDriver_init(struct IFileDriverImplWin32 *This)
{
	if(!This->inited) {
		SYSTEM_INFO sysinfo;
		GetNativeSystemInfo(&sysinfo);
		This->alloc_gran = sysinfo.dwAllocationGranularity;
		This->inited = true;
	}
	return SUCCESS;
}

static inline int createfile_mode_from_IoMode(IoMode flags, int *create_dispo) {
	int createfile_mode = 0;

	if(flags & read_mode)
		createfile_mode |= GENERIC_READ;
	if(flags & write_mode)
		createfile_mode |= GENERIC_WRITE;
	if(flags & create_mode)
		*create_dispo = CREATE_ALWAYS;
	else
		*create_dispo = OPEN_EXISTING;
	return createfile_mode;
}

static generic_error IFileDriver_Open(struct IFileDriver *iface, const char *file_path, IoMode io_mode, struct IDriverFile **file)
{
	struct IFileDriverImplWin32 *This = impl_from_IFileDriver(iface);
	struct IDriverFileImplWin32 *file_impl;
	int open_mode, create_dispo;
	HANDLE file_handle;

	if(!iface || !file_path || !file
			|| (io_mode & ~(create_mode | read_mode | write_mode)) /* Reject invalid flags */
			|| !(io_mode & (read_mode | write_mode))) /* read_mode or write_mode must always be specified */
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		generic_error ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	file_impl = malloc(sizeof(*file_impl));
	if(!file_impl)
		return -ERR_NOMEM;

	open_mode = createfile_mode_from_IoMode(io_mode, &create_dispo);
	file_handle = CreateFileA(file_path,
			open_mode,
			FILE_SHARE_READ,
			NULL,
			create_dispo,
			FILE_ATTRIBUTE_NORMAL,
			NULL
		);
	if(file_handle == INVALID_HANDLE_VALUE) {
		DWORD err = GetLastError();
		free(file_impl);
		switch(err) {
			case ERROR_FILE_NOT_FOUND: return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}

	if(!This->files) {
		This->files = file_impl;
		file_impl->prev = NULL;
		file_impl->next = NULL;
	} else {
		file_impl->prev = NULL;
		file_impl->next = This->files;
		This->files->prev = file_impl;
		This->files = file_impl;
	}

	file_impl->iface.lpVtbl = &DriverFileVtbl;
	file_impl->iface.IFileDriver_iface = &This->iface;
	file_impl->alloc_gran = This->alloc_gran;
	file_impl->io_mode = io_mode;
	if(io_mode & read_mode) {
		file_impl->map_prot = PAGE_READONLY;
		file_impl->map_access = FILE_MAP_READ;
	} else if(io_mode & write_mode) {
		file_impl->map_prot = PAGE_READWRITE;
		file_impl->map_access = FILE_MAP_WRITE;
	}
	file_impl->file_handle = file_handle;
	file_impl->map_handle = NULL;
	file_impl->map_len = 0;
	file_impl->mappings = NULL;
	*file = &file_impl->iface;
	return SUCCESS;
}

static generic_error IFileDriver_Closeall(struct IFileDriver *iface)
{
	struct IFileDriverImplWin32 *This = impl_from_IFileDriver(iface);
	int ret;

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	while(This->files)
		DriverFileVtbl.Close(&This->files->iface);

	return SUCCESS;
}

static generic_error IFileDriver_Remove(struct IFileDriver *iface, const char *file_path)
{
	struct IFileDriverImplWin32 *This = impl_from_IFileDriver(iface);
	int ret;
	BOOL bret;

	if(!iface || !file_path)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	bret = RemoveDirectoryA(file_path);
	if(bret)
		return SUCCESS;

	bret = DeleteFileA(file_path);
	if(!bret) {
		DWORD err = GetLastError();
		switch(err) {
			case ERROR_FILE_NOT_FOUND: return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IFileDriver_IsMmapSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplWin32 *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return SUCCESS;
}

static generic_error IFileDriver_IsStreamSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplWin32 *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return SUCCESS;
}

static struct IFileDriverVtbl FileDriverVtbl = {
	IFileDriver_Open,
	IFileDriver_Closeall,
	IFileDriver_Remove,
	IFileDriver_IsMmapSupported,
	IFileDriver_IsStreamSupported
};

static struct IFileDriverImplWin32 FileDriverImpl = {
	{ &FileDriverVtbl },
	false,
	-1,
	NULL
};

const struct IFileDriver *FileDriver_win32_ptr = &FileDriverImpl.iface;


#ifndef MMAPBUFDIO_TYPES_H
#define MMAPBUFDIO_TYPES_H

/**
 * \file
 * \brief OS-specific type declarations & portable macros
 */

/* Import Windows types */
#ifdef _WIN32
#  include <windef.h>
#endif /* _WIN32 */

/* inline keyword */
#if !defined(__STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
#  define inline
#endif /* __STDC_VERSION__ */

/* Standard c99 types & Win32 replacements */
#if !defined(_MSC_VER) || (_MSC_VER >= 1600)
#  include <stdint.h>
#else
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
typedef LONG_PTR intptr_t;
typedef ULONG_PTR uintptr_t;
#endif /* _MSC_VER */

/* Boolean */
#ifndef __cplusplus
typedef enum {
    false = 0,
    true = 1
} Boolean;
#else
typedef bool Boolean;
#endif /* __cplusplus */

/* Extracts a structure pointer from the address of a member */
#ifndef CONTAINING_RECORD
#  define CONTAINING_RECORD(address, type, field) \
    ((type *)( \
        (char *)(address) - \
        (uintptr_t)(&((type *)0)->field) \
    ))
#endif /* CONTAINING_RECORD */

/* Make GCC happy with unused parameters... */
#define mark_used(param) ((void)param)

#endif /* MMAPBUFDIO_TYPES_H */

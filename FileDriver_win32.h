#ifndef MMAPBUFDIO_FILEDRIVER_WIN32_H
#define MMAPBUFDIO_FILEDRIVER_WIN32_H

/**
 * \file
 * \brief Public export of Windows-compatible IFileDriver implementation
 */

#include "IFileDriver.h"

extern const struct IFileDriver *FileDriver_win32_ptr;

#endif /* MMAPBUFDIO_FILEDRIVER_WIN32_H */

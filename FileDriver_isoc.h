#ifndef MMAPBUFDIO_FILEDRIVER_ISOC_H
#define MMAPBUFDIO_FILEDRIVER_ISOC_H

/**
 * \file
 * \brief Public export of ISO C89 (extended)-compatible IFileDriver implementation
 */

#include "IFileDriver.h"

extern const struct IFileDriver *FileDriver_isoc_ptr;

#endif /* MMAPBUFDIO_FILEDRIVER_ISOC_H */

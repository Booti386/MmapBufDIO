#ifndef MMAPBUFDIO_PLATFORM_H
#define MMAPBUFDIO_PLATFORM_H

/* !!! Must be the first included header !!! */

/**
 * \file
 * \brief OS-specific macro declarations
 */

/* 64 bits off_t & ftello() */
#ifndef __USE_LARGEFILE64
#  define __USE_LARGEFILE64
#endif
#ifndef __USE_FILE_OFFSET64
#  define __USE_FILE_OFFSET64
#endif
#ifndef _LARGEFILE_SOURCE
#  define _LARGEFILE_SOURCE
#endif
#ifndef _LARGEFILE64_SOURCE
#  define _LARGEFILE64_SOURCE
#endif
#ifdef  _FILE_OFFSET_BITS
#  undef  _FILE_OFFSET_BITS
#endif
#define _FILE_OFFSET_BITS 64
#include <stdio.h>

#include "types.h"

#endif /* MMAPBUFDIO_PLATFORM_H */

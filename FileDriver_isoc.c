/**
 * \file
 * \brief ISO C89 (extended)-compatible IFileDriver implementation
 */

#include "platform.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "types.h"
#include "error.h"
#include "IFileDriver.h"
#include "FileDriver_isoc.h"

/**
 * \class IDriverFileImplIsoC
 * \implements IDriverFile
 * \brief ISO C89 (extended)-compatible implementation of IDriverFile interface.
 */
struct IDriverFileImplIsoC {
	struct IDriverFile iface;

	struct IDriverFileImplIsoC *prev, *next;
	IoMode io_mode;
	FILE *fp;
};

/**
 * \class IFileDriverImplIsoC
 * \implements IFileDriver
 * \brief ISO C89 (extended)-compatible implementation of IFileDriver interface.
 */
struct IFileDriverImplIsoC {
	struct IFileDriver iface;

	struct IDriverFileImplIsoC *files;
};

static struct IDriverFileVtbl DriverFileVtbl;
static struct IFileDriverVtbl FileDriverVtbl;
static struct IFileDriverImplIsoC FileDriverImpl;

static inline int64_t ftell_platform(FILE *fp) {
#ifdef _WIN32
	return (int64_t)_ftelli64(fp);
#else
	return (int64_t)ftello(fp);
#endif /* _WIN32 */
}

static inline int fseek_platform(FILE *fp, int64_t off, int origin) {
#ifdef _WIN32
	return _fseeki64(fp, (int64_t)off, origin);
#else
	return fseeko(fp, (int64_t)off, origin);
#endif /* _WIN32 */
}

static inline FILE *fopen_platform(const char *path, const char *mode) {
#ifdef _WIN32
	FILE *fp;
	errno = fopen_s(&fp, path, mode);
	if(errno)
		fp = NULL;
	return fp;
#else
	return fopen(path, mode);
#endif /* _WIN32 */
}

/******* IDriverFile functions *******/

static inline struct IDriverFileImplIsoC *impl_from_IDriverFile(struct IDriverFile *iface) {
	return CONTAINING_RECORD(iface, struct IDriverFileImplIsoC, iface);
}

static generic_error IDriverFile_Close(struct IDriverFile *iface)
{
	struct IDriverFileImplIsoC *This = impl_from_IDriverFile(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	fclose(This->fp);
	if(This->next)
		This->next->prev = This->prev;
	if(This->prev)
		This->prev->next = This->next;
	else /* If This is the first entry. */
		FileDriverImpl.files = This->next;
	free(This);
	return SUCCESS;
}

static generic_error IDriverFile_GetSize(struct IDriverFile *iface, uint64_t *dst_size)
{
	struct IDriverFileImplIsoC *This = impl_from_IDriverFile(iface);
	int64_t old_off, size;
	int ret;

	if(!iface || !dst_size)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	errno = 0;
	old_off = ftell_platform(This->fp);
	if(old_off < 0) goto error;

	errno = 0;
	ret = fseek_platform(This->fp, 0L, SEEK_END);
	if(ret < 0) goto error;

	errno = 0;
	size = ftell_platform(This->fp);
	if(size < 0) goto error;

	errno = 0;
	ret = fseek_platform(This->fp, old_off, SEEK_SET);
	if(ret < 0) goto error;

	*dst_size = (uint64_t)size;
	return SUCCESS;

	error:
	switch(errno) {
		case EINVAL: return -ERR_INVL;
		case ENOMEM: return -ERR_NOMEM;
		case EBADF:  return -ERR_IO;
	}
	return -ERR_UNK;
}

static generic_error IDriverFile_Mmap(struct IDriverFile *iface, uint64_t off, uint64_t len, void **map_ptr)
{
	mark_used(iface);
	mark_used(off);
	mark_used(len);
	mark_used(map_ptr);
	return -ERR_NOTIMPL;
}

static generic_error IDriverFile_Mflush(struct IDriverFile *iface, void *map_ptr)
{
	mark_used(iface);
	mark_used(map_ptr);
	return -ERR_NOTIMPL;
}

static generic_error IDriverFile_Munmap(struct IDriverFile *iface, void *map_ptr)
{
	mark_used(iface);
	mark_used(map_ptr);
	return -ERR_NOTIMPL;
}

static generic_error IDriverFile_Read(struct IDriverFile *iface, uint64_t off, uint64_t len, void *dst_ptr)
{
	struct IDriverFileImplIsoC *This = impl_from_IDriverFile(iface);
	int ret;
	size_t nelems;

	if(!iface || !len || !dst_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & read_mode))
		return -ERR_INVL;

	errno = 0;
	ret = fseek_platform(This->fp, (int64_t)off, SEEK_SET);
	if(ret < 0) {
		switch(errno) {
			case EINVAL:    return -ERR_INVL;
			case ENOMEM:    return -ERR_NOMEM;
			case EOVERFLOW:
			case EBADF:     return -ERR_IO;
		}
		return -ERR_UNK;
	}
	nelems = fread(dst_ptr, len, 1, This->fp);
	if(nelems != 1) {
		if(ferror(This->fp))
			return -ERR_IO;
		if(feof(This->fp))
			return -ERR_EOF;
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IDriverFile_Write(struct IDriverFile *iface, uint64_t off, uint64_t len, void *src_ptr)
{
	struct IDriverFileImplIsoC *This = impl_from_IDriverFile(iface);
	int ret;
	size_t nelems;

	if(!iface || !len || !src_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & write_mode))
		return -ERR_INVL;

	errno = 0;
	ret = fseek_platform(This->fp, (int64_t)off, SEEK_SET);
	if(ret < 0) {
		switch(errno) {
			case EINVAL: return -ERR_INVL;
			case ENOMEM: return -ERR_NOMEM;
			case EBADF:  return -ERR_IO;
		}
		return -ERR_UNK;
	}
	nelems = fwrite(src_ptr, len, 1, This->fp);
	if(nelems != 1) {
		if(ferror(This->fp))
			return -ERR_IO;
		if(feof(This->fp))
			return -ERR_EOF; /* Don't know what it could mean in this case... */
		return -ERR_UNK;
	}
	fflush(This->fp);
	return SUCCESS;
}

static struct IDriverFileVtbl DriverFileVtbl = {
	IDriverFile_Close,
	IDriverFile_GetSize,
	IDriverFile_Mmap,
	IDriverFile_Mflush,
	IDriverFile_Munmap,
	IDriverFile_Read,
	IDriverFile_Write
};


/******* IFileDriver functions *******/

static inline struct IFileDriverImplIsoC *impl_from_IFileDriver(struct IFileDriver *iface) {
	return CONTAINING_RECORD(iface, struct IFileDriverImplIsoC, iface);
}

#define FOPEN_MODE_STR_LEN 4
static inline void fopen_mode_from_IoMode(char *fopen_mode, IoMode flags) {
	int shift = 0;
	if(flags & create_mode) {
		fopen_mode[shift++] = 'w';
		if(flags & read_mode)
			fopen_mode[shift++] = '+';
	} else {
		fopen_mode[shift++] = 'r';
		if(flags & write_mode)
			fopen_mode[shift++] = '+';
	}
	fopen_mode[shift++] = 'b';
	fopen_mode[shift] = '\0';
}

static generic_error IFileDriver_Open(struct IFileDriver *iface, const char *file_path, IoMode io_mode, struct IDriverFile **file)
{
	struct IFileDriverImplIsoC *This = impl_from_IFileDriver(iface);
	struct IDriverFileImplIsoC *file_impl;
	char fopen_mode[FOPEN_MODE_STR_LEN];
	FILE *fp;

	if(!iface || !file_path || !file
			|| (io_mode & ~(create_mode | read_mode | write_mode)) /* Reject invalid flags */
			|| !(io_mode & (read_mode | write_mode))) /* read_mode or write_mode must always be specified */
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	file_impl = malloc(sizeof(*file_impl));
	if(!file_impl)
		return -ERR_NOMEM;

	fopen_mode_from_IoMode(fopen_mode, io_mode);
	errno = 0;
	fp = fopen_platform(file_path, fopen_mode);
	if(!fp) {
		free(file_impl);
		switch(errno) {
			case EINVAL: return -ERR_INVL;
			case ENOMEM: return -ERR_NOMEM;
			case EPERM:
			case EACCES: return -ERR_PERMS;
			case ENOSPC: return -ERR_NOSPC;
			case ENOENT: return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}

	if(!This->files) {
		This->files = file_impl;
		file_impl->prev = NULL;
		file_impl->next = NULL;
	} else {
		file_impl->prev = NULL;
		file_impl->next = This->files;
		This->files->prev = file_impl;
		This->files = file_impl;
	}

	file_impl->iface.lpVtbl = &DriverFileVtbl;
	file_impl->iface.IFileDriver_iface = &This->iface;
	file_impl->io_mode = io_mode;
	file_impl->fp = fp;
	*file = &file_impl->iface;
	return SUCCESS;
}

static generic_error IFileDriver_Closeall(struct IFileDriver *iface)
{
	struct IFileDriverImplIsoC *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	while(This->files)
		DriverFileVtbl.Close(&This->files->iface);

	return SUCCESS;
}

static generic_error IFileDriver_Remove(struct IFileDriver *iface, const char *file_path)
{
	struct IFileDriverImplIsoC *This = impl_from_IFileDriver(iface);
	int ret;

	if(!iface || !file_path)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	errno = 0;
	ret = remove(file_path);
	if(ret < 0) {
		switch(errno) {
			case EFAULT:
			case ENAMETOOLONG:
			case EINVAL:       return -ERR_INVL;
			case ENOMEM:       return -ERR_NOMEM;
			case EACCES:
			case EPERM:        return -ERR_PERMS;
			case EISDIR:
			case ENOTDIR:
			case ENOTEMPTY:
			case EROFS:
			case ELOOP:
			case EBUSY:
			case EIO:          return -ERR_IO;
			case ENOENT:       return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IFileDriver_IsMmapSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplIsoC *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return -ERR_NOTIMPL;
}

static generic_error IFileDriver_IsStreamSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplIsoC *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return SUCCESS;
}

static struct IFileDriverVtbl FileDriverVtbl = {
	IFileDriver_Open,
	IFileDriver_Closeall,
	IFileDriver_Remove,
	IFileDriver_IsMmapSupported,
	IFileDriver_IsStreamSupported
};

static struct IFileDriverImplIsoC FileDriverImpl = {
	{ &FileDriverVtbl },
	NULL
};

const struct IFileDriver *FileDriver_isoc_ptr = &FileDriverImpl.iface;

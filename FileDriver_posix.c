/**
 * \file
 * \brief POSIX-compatible IFileDriver implementation
 * \namespace FileDriver_posix
 */

#include "platform.h"

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>

#include "types.h"
#include "error.h"
#include "IFileDriver.h"
#include "FileDriver_posix.h"

/**
 * \class IDriverFileImplPosix
 * \implements IDriverFile
 */
struct IDriverFileImplPosix {
	struct IDriverFile iface;

	struct IDriverFileImplPosix *prev, *next;
	long page_size;
	IoMode io_mode;
	int map_prot;
	int fd;
	struct map_entry {
		struct map_entry *prev, *next;
		void *real_ptr;
		uint64_t real_len;
		void *ptr;
	} *mappings;
};

/**
 * \class IFileDriverImplPosix
 * \implements IFileDriver
 */
struct IFileDriverImplPosix {
	struct IFileDriver iface;

	Boolean inited;
	long page_size;
	struct IDriverFileImplPosix *files;
};

static struct IDriverFileVtbl DriverFileVtbl;
static struct IFileDriverVtbl FileDriverVtbl;
static struct IFileDriverImplPosix FileDriverImpl;

/******* IDriverFile functions *******/

static inline struct IDriverFileImplPosix *impl_from_IDriverFile(struct IDriverFile *iface) {
	return CONTAINING_RECORD(iface, struct IDriverFileImplPosix, iface);
}

static generic_error IDriverFile_Close(struct IDriverFile *iface)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	while(This->mappings)
		DriverFileVtbl.Munmap(iface, This->mappings->ptr);
	close(This->fd);
	if(This->next)
		This->next->prev = This->prev;
	if(This->prev)
		This->prev->next = This->next;
	else /* If This is the first entry. */
		FileDriverImpl.files = This->next;
	free(This);
	return SUCCESS;
}

static generic_error IDriverFile_GetSize(struct IDriverFile *iface, uint64_t *dst_size)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	struct stat stat_buf;
	int ret;

	if(!iface || !dst_size)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	errno = 0;
	ret = fstat(This->fd, &stat_buf);
	if(ret < 0) {
		switch(errno) {
			case EINVAL:    return -ERR_INVL;
			case ENOMEM:    return -ERR_NOMEM;
			case EOVERFLOW:
			case EBADF:     return -ERR_IO;
		}
		return -ERR_UNK;
	}
	if(stat_buf.st_size < 0)
		return -ERR_UNK;

	*dst_size = (uint64_t)stat_buf.st_size;
	return SUCCESS;
}

static generic_error IDriverFile_Mmap(struct IDriverFile *iface, uint64_t off, uint64_t len, void **map_ptr)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	uint64_t page_idx;
	void *ptr;
	struct map_entry *mapping;

	if(!iface || !len || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	page_idx = off / This->page_size;
	off -= page_idx * This->page_size;
	errno = 0;
	ptr = mmap(NULL, len + off, This->map_prot, MAP_PRIVATE, This->fd, page_idx);
	if(ptr == MAP_FAILED) {
		switch(errno) {
			case EINVAL:    return -ERR_INVL;
			case ENOMEM:    return -ERR_NOMEM;
			case EPERM:
			case EACCES:    return -ERR_PERMS;
			case EOVERFLOW: return -ERR_OVERFLOW;
			case ENODEV:
			case EBADF:     return -ERR_IO;
		}
		return -ERR_NOMEM;
	}
	mapping = malloc(sizeof(*mapping));
	if(!mapping) {
		munmap(ptr, len + off);
		return -ERR_NOMEM;
	}
	if(!This->mappings) {
		This->mappings = mapping;
		mapping->prev = NULL;
		mapping->next = NULL;
	} else {
		mapping->prev = NULL;
		mapping->next = This->mappings;
		This->mappings->prev = mapping;
		This->mappings = mapping;
	}
	mapping->real_ptr = ptr;
	mapping->real_len = len + off;
	mapping->ptr = (void *)((intptr_t)ptr + (intptr_t)off);
	*map_ptr = mapping->ptr;
	return SUCCESS;
}

static generic_error IDriverFile_Mflush(struct IDriverFile *iface, void *map_ptr)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	struct map_entry *mapping;
	int ret;

	if(!iface || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	mapping = This->mappings;
	while(mapping) {
		if(mapping->ptr == map_ptr)
			break;
		mapping = mapping->next;
	}
	if(!mapping)
		return -ERR_INVL;

	errno = 0;
	ret = msync(mapping->real_ptr, mapping->real_len, MS_SYNC | MS_INVALIDATE);
	if(ret < 0) {
		switch(errno) {
			case ENOMEM:
			case EINVAL: return -ERR_INVL;
			case EBUSY:  return -ERR_BUSY;
		}
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IDriverFile_Munmap(struct IDriverFile *iface, void *map_ptr)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	struct map_entry *mapping;
	int ret;

	if(!iface || !map_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;

	mapping = This->mappings;
	while(mapping) {
		if(mapping->ptr == map_ptr)
			break;
		mapping = mapping->next;
	}
	if(!mapping)
		return -ERR_INVL;

	errno = 0;
	ret = munmap(mapping->real_ptr, mapping->real_len);
	if(ret < 0) {
		switch(errno) {
			case EINVAL: return -ERR_INVL;
		}
		return -ERR_UNK;
	}

	if(mapping->next)
		mapping->next->prev = mapping->prev;
	if(mapping->prev)
		mapping->prev->next = mapping->next;
	else /* If mapping is the first entry. */
		This->mappings = mapping->next;
	free(mapping);
	return SUCCESS;
}

static generic_error IDriverFile_Read(struct IDriverFile *iface, uint64_t off, uint64_t len, void *dst_ptr)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	off_t ret_off;
	ssize_t size_read;

	if(!iface || !len || !dst_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & read_mode))
		return -ERR_INVL;

	errno = 0;
	ret_off = lseek(This->fd, (int64_t)off, SEEK_SET);
	if(ret_off < 0) {
		switch(errno) {
			case EINVAL:    return -ERR_INVL;
			case EOVERFLOW:
			case EBADF:     return -ERR_IO;
		}
		return -ERR_UNK;
	}
	if(ret_off != (int64_t)off)
		return -ERR_UNK;

	while(len > 0) {
		errno = 0;
		size_read = read(This->fd, dst_ptr, len);
		if(size_read < 0) {
			switch(errno) {
				case EFAULT:
				case EINVAL: return -ERR_INVL;
				case EIO:
				case EINTR:
				case EBADF:  return -ERR_IO;
			}
			return -ERR_UNK;
		}
		if(size_read == 0)
			return -ERR_EOF;
		if((uint64_t)size_read > len)
			break; /* Not sure... */
		len -= size_read;
		dst_ptr = (void *)((intptr_t)dst_ptr + size_read);
	}
	return SUCCESS;
}

static generic_error IDriverFile_Write(struct IDriverFile *iface, uint64_t off, uint64_t len, void *src_ptr)
{
	struct IDriverFileImplPosix *This = impl_from_IDriverFile(iface);
	off_t ret_off;
	ssize_t size_written;

	if(!iface || !len || !src_ptr)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &DriverFileVtbl)
		return -ERR_WRONG_IFACE;
	if(!(This->io_mode & write_mode))
		return -ERR_INVL;

	errno = 0;
	ret_off = lseek(This->fd, (int64_t)off, SEEK_SET);
	if(ret_off < 0) {
		switch(errno) {
			case EINVAL:    return -ERR_INVL;
			case EOVERFLOW:
			case EBADF:     return -ERR_IO;
		}
		return -ERR_UNK;
	}
	if(ret_off != (int64_t)off)
		return -ERR_UNK;

	while(len > 0) {
		errno = 0;
		size_written = write(This->fd, src_ptr, len);
		if(size_written <= 0) {
			switch(errno) {
				case EFAULT:
				case EINVAL: return -ERR_INVL;
				case ENOSPC: return -ERR_NOSPC;
				case EIO:
				case EINTR:
				case EBADF:  return -ERR_IO;
			}
			return -ERR_UNK;
		}
		if((uint64_t)size_written > len)
			break; /* Not sure... */
		len -= size_written;
		src_ptr = (void *)((intptr_t)src_ptr + size_written);
	}
	return SUCCESS;
}

static struct IDriverFileVtbl DriverFileVtbl = {
	IDriverFile_Close,
	IDriverFile_GetSize,
	IDriverFile_Mmap,
	IDriverFile_Mflush,
	IDriverFile_Munmap,
	IDriverFile_Read,
	IDriverFile_Write
};


/******* IFileDriver functions *******/

static inline struct IFileDriverImplPosix *impl_from_IFileDriver(struct IFileDriver *iface) {
	return CONTAINING_RECORD(iface, struct IFileDriverImplPosix, iface);
}

static generic_error FileDriver_init(struct IFileDriverImplPosix *This)
{
	if(!This->inited) {
		This->page_size = sysconf(_SC_PAGESIZE);
		if(This->page_size <= 0)
			return -ERR_UNK;
		This->inited = true;
	}
	return SUCCESS;
}

static inline int open_mode_from_IoMode(IoMode flags) {
	int open_mode = 0;

	if((flags & read_mode)
			&& (flags & write_mode))
		open_mode = O_RDWR;
	else if(flags & read_mode)
		open_mode = O_RDONLY;
	else if(flags & write_mode)
		open_mode = O_WRONLY;
	if(flags & create_mode)
		open_mode |= O_CREAT | O_TRUNC;
	return open_mode;
}

static generic_error IFileDriver_Open(struct IFileDriver *iface, const char *file_path, IoMode io_mode, struct IDriverFile **file)
{
	struct IFileDriverImplPosix *This = impl_from_IFileDriver(iface);
	struct IDriverFileImplPosix *file_impl;
	int open_mode;
	int fd;

	if(!iface || !file_path || !file
			|| (io_mode & ~(create_mode | read_mode | write_mode)) /* Reject invalid flags */
			|| !(io_mode & (read_mode | write_mode))) /* read_mode or write_mode must always be specified */
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		generic_error ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	file_impl = malloc(sizeof(*file_impl));
	if(!file_impl)
		return -ERR_NOMEM;

	open_mode = open_mode_from_IoMode(io_mode);
	errno = 0;
	fd = open(file_path, open_mode, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if(fd < 0) {
		free(file_impl);
		switch(errno) {
			case EINVAL: return -ERR_INVL;
			case ENOMEM: return -ERR_NOMEM;
			case EPERM:
			case EACCES: return -ERR_PERMS;
			case ENOSPC: return -ERR_NOSPC;
			case ENOENT: return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}

	if(!This->files) {
		This->files = file_impl;
		file_impl->prev = NULL;
		file_impl->next = NULL;
	} else {
		file_impl->prev = NULL;
		file_impl->next = This->files;
		This->files->prev = file_impl;
		This->files = file_impl;
	}

	file_impl->iface.lpVtbl = &DriverFileVtbl;
	file_impl->iface.IFileDriver_iface = &This->iface;
	file_impl->page_size = This->page_size;
	file_impl->io_mode = io_mode;
	file_impl->map_prot = PROT_NONE;
	if(io_mode & read_mode) file_impl->map_prot |= PROT_READ;
	if(io_mode & write_mode) file_impl->map_prot |= PROT_WRITE;
	file_impl->fd = fd;
	file_impl->mappings = NULL;
	*file = &file_impl->iface;
	return SUCCESS;
}

static generic_error IFileDriver_Closeall(struct IFileDriver *iface)
{
	struct IFileDriverImplPosix *This = impl_from_IFileDriver(iface);
	int ret;

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	while(This->files)
		DriverFileVtbl.Close(&This->files->iface);

	return SUCCESS;
}

static generic_error IFileDriver_Remove(struct IFileDriver *iface, const char *file_path)
{
	struct IFileDriverImplPosix *This = impl_from_IFileDriver(iface);
	int ret;

	if(!iface || !file_path)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;

	if(!This->inited) {
		ret = FileDriver_init(This);
		if(ret < 0)
			return ret;
	}

	errno = 0;
	ret = rmdir(file_path);
	if(ret < 0) {
		switch(errno) {
			case ENOTDIR:      break;
			case EFAULT:
			case ENAMETOOLONG:
			case EINVAL:       return -ERR_INVL;
			case ENOMEM:       return -ERR_NOMEM;
			case EACCES:
			case EPERM:        return -ERR_PERMS;
			case ENOTEMPTY:
			case EROFS:
			case ELOOP:
			case EBUSY:        return -ERR_IO;
			case ENOENT:       return -ERR_NOTEXIST;
			default:           return -ERR_UNK;
		}
	} else
		return SUCCESS;

	errno = 0;
	ret = unlink(file_path);
	if(ret < 0) {
		switch(errno) {
			case EFAULT:
			case ENAMETOOLONG:
			case EINVAL:       return -ERR_INVL;
			case ENOMEM:       return -ERR_NOMEM;
			case EACCES:
			case EPERM:        return -ERR_PERMS;
			case EISDIR:
			case ENOTDIR:
			case ENOTEMPTY:
			case EROFS:
			case ELOOP:
			case EBUSY:
			case EIO:          return -ERR_IO;
			case ENOENT:       return -ERR_NOTEXIST;
		}
		return -ERR_UNK;
	}
	return SUCCESS;
}

static generic_error IFileDriver_IsMmapSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplPosix *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return SUCCESS;
}

static generic_error IFileDriver_IsStreamSupported(struct IFileDriver *iface)
{
	struct IFileDriverImplPosix *This = impl_from_IFileDriver(iface);

	if(!iface)
		return -ERR_INVL;
	if(This->iface.lpVtbl != &FileDriverVtbl)
		return -ERR_WRONG_IFACE;
	return SUCCESS;
}

static struct IFileDriverVtbl FileDriverVtbl = {
	IFileDriver_Open,
	IFileDriver_Closeall,
	IFileDriver_Remove,
	IFileDriver_IsMmapSupported,
	IFileDriver_IsStreamSupported
};

static struct IFileDriverImplPosix FileDriverImpl = {
	{ &FileDriverVtbl },
	false,
	-1,
	NULL
};

const struct IFileDriver *FileDriver_posix_ptr = &FileDriverImpl.iface;

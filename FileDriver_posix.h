#ifndef MMAPBUFDIO_FILEDRIVER_POSIX_H
#define MMAPBUFDIO_FILEDRIVER_POSIX_H

/**
 * \file
 * \brief Public export of POSIX-compatible IFileDriver implementation
 */

#include "IFileDriver.h"

extern const struct IFileDriver *FileDriver_posix_ptr;

#endif /* MMAPBUFDIO_FILEDRIVER_POSIX_H */

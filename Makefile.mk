SUBDIRS-y :=

libmmapbufdio := libmmapbufdio$(DYN_LIB_EXT)
test-drvs := test-drvs$(EXE_EXT)
BINS-y := $(libmmapbufdio) $(test-drvs)

OBJS-$(libmmapbufdio)-y := \
    FileInterface.c.o \
    FileDriver_isoc.c.o
OBJS-$(libmmapbufdio)-$(CONFIG_OS_LINUX) += FileDriver_posix.c.o
OBJS-$(libmmapbufdio)-$(CONFIG_OS_WINDOWS) += FileDriver_win32.c.o

CFLAGS-$(CONFIG_OS_LINUX) += $(CFLAGS_SHARED)
CFLAGS-$(CONFIG_OS_MACOSX) += $(CFLAGS_SHARED)

LDFLAGS-$(libmmapbufdio)-y += $(LDFLAGS_SHARED)

OBJS-$(test-drvs)-y := test-drvs.c.o

LIBS-$(test-drvs)-y := $(libmmapbufdio)

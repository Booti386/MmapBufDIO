#ifndef MMAPBUFDIO_FILEINTERFACE_H
#define MMAPBUFDIO_FILEINTERFACE_H

/**
 * \file
 * \brief Unified interface declarations to use IFileDrivers functionnalities
 */

#include "types.h"
#include "IFileDriver.h"

#define invalid_filepos ((uint64_t)-1);

struct FileInterface;

/**
 *
 */
typedef enum {
	DRIVER_POSIX = 0,
	DRIVER_WIN32,
	DRIVER_ISOC,
	DRIVER_MAX
} driver_id;

/**
 *
 */
typedef enum {
    /* file i/o operations - only one of them may be specified */
    standard_io      = 0 << 0,     /**< If set use simple read/writes */
    memory_mapped_io = 1 << 0,     /**< If set use MMIO */
    io_opt_mask      = 1 << 0,

	/* file i/o flags - An OR combination of them may be specified */
	io_flag_none     = 0,
    io_flag_buffered = 1 << 1 /**< If set use buffered IO */
} IoOpts;

/**
 *
 */
typedef enum {
	beginning = 0,
    current,
    ending
} OriginType;

extern generic_error fiCreate(struct FileInterface **fi, driver_id drvid);
extern generic_error fiDestroy(struct FileInterface **fi);
extern generic_error fiIsOptsSupported(struct FileInterface *fi, IoOpts opt);

#endif /* MMAPBUFDIO_FILEINTERFACE_H */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FileDriver_isoc.h"
#ifdef _WIN32
#  include "FileDriver_win32.h"
#else
#  include "FileDriver_posix.h"
#endif /* _WIN32 */

#define str_sec(str, len) (!(str) ? (int)sizeof("(null)") - 1 : len), (!(str) ? "(null)" : str)

#define callme(This, func, ...) This->lpVtbl->func(This, ## __VA_ARGS__)
#define callme0(This, func) This->lpVtbl->func(This)
#define VOIDPTR(ptr) ((void *)ptr)

#define BUF_LEN 4
static void test_drv(struct IFileDriver *drv)
{
	const char *non_exist_filename = "non-existent-file";
	char test_buf[BUF_LEN] = "test";
	char test_buf_read[BUF_LEN] = {0};
	void *test_mmap_ptr = NULL, *test_mmap_ptr1 = NULL;
	struct IDriverFile *file = NULL;
	uint64_t size = 0;
	int ret;

	ret = callme(drv, Remove, non_exist_filename);
	printf("(%p)->Remove(%p, \"%s\") => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, ret);

	ret = callme(drv, Open, non_exist_filename, 0, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, 0, VOIDPTR(&file), ret);
	ret = callme(drv, Open, non_exist_filename, create_mode, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, create_mode, VOIDPTR(&file), ret);

	ret = callme(drv, Open, non_exist_filename, read_mode, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, read_mode, VOIDPTR(&file), ret);
	ret = callme(drv, Open, non_exist_filename, write_mode, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, write_mode, VOIDPTR(&file), ret);
	ret = callme(drv, Open, non_exist_filename, read_mode | write_mode, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, read_mode | write_mode, VOIDPTR(&file), ret);

	ret = callme(drv, Open, non_exist_filename, create_mode | write_mode | read_mode, &file);
	printf("(%p)->Open(%p, \"%s\", %d, %p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, create_mode | write_mode | read_mode, VOIDPTR(&file), ret);

	ret = callme0(drv, IsStreamSupported);
	if(ret == SUCCESS) {
		ret = callme(file, GetSize, &size);
		printf("(%p)->GetSize(%p, *%p = %d) => %d\n", VOIDPTR(file), VOIDPTR(file), VOIDPTR(&size), (int)size, ret);
		ret = callme(file, Write, 0, BUF_LEN, test_buf);
		printf("(%p)->Write(%p, %d, %d, \"%.*s\") => %d\n", VOIDPTR(file), VOIDPTR(file), 0, BUF_LEN, BUF_LEN, test_buf, ret);
		ret = callme(file, GetSize, &size);
		printf("(%p)->GetSize(%p, *%p = %d) => %d\n", VOIDPTR(file), VOIDPTR(file), VOIDPTR(&size), (int)size, ret);
		ret = callme(file, Read, 0, BUF_LEN, test_buf_read);
		printf("(%p)->Read(%p, %d, %d, \"%.*s\") => %d\n", VOIDPTR(file), VOIDPTR(file), 0, BUF_LEN, BUF_LEN, test_buf_read, ret);
		ret = callme(file, Read, 1, BUF_LEN - 1, test_buf_read);
		printf("(%p)->Read(%p, %d, %d, \"%.*s\") => %d\n", VOIDPTR(file), VOIDPTR(file), 1, BUF_LEN - 1, BUF_LEN - 1, test_buf_read, ret);
		ret = callme(file, Mmap, 0, BUF_LEN, &test_mmap_ptr);
	} else
		printf("File streaming functions not supported, skipping tests...\n");

	ret = callme0(drv, IsMmapSupported);
	if(ret == SUCCESS) {
		printf("(%p)->Mmap(%p, %d, %d, *%p = %p = \"%.*s\") => %d\n", VOIDPTR(file), VOIDPTR(file), 0, BUF_LEN, VOIDPTR(&test_mmap_ptr), test_mmap_ptr, str_sec((char *)test_mmap_ptr, BUF_LEN), ret);
		ret = callme(file, Mmap, 1, BUF_LEN - 1, &test_mmap_ptr1);
		printf("(%p)->Mmap(%p, %d, %d, *%p = %p = \"%.*s\") => %d\n", VOIDPTR(file), VOIDPTR(file), 1, BUF_LEN - 1, VOIDPTR(&test_mmap_ptr1), test_mmap_ptr1, str_sec((char *)test_mmap_ptr1, BUF_LEN - 1), ret);
		ret = callme(file, Munmap, test_mmap_ptr);
		printf("(%p)->Munmap(%p, %p) => %d\n", VOIDPTR(file), VOIDPTR(file), test_mmap_ptr, ret);
		ret = callme(file, Munmap, test_mmap_ptr1);
		printf("(%p)->Munmap(%p, %p) => %d\n", VOIDPTR(file), VOIDPTR(file), test_mmap_ptr1, ret);
		ret = callme(file, Munmap, test_mmap_ptr);
		printf("(%p)->Munmap(%p, %p) => %d\n", VOIDPTR(file), VOIDPTR(file), test_mmap_ptr, ret);
		ret = callme(file, Munmap, test_mmap_ptr1);
		printf("(%p)->Munmap(%p, %p) => %d\n", VOIDPTR(file), VOIDPTR(file), test_mmap_ptr1, ret);
	} else
		printf("File mapping functions not supported, skipping tests...\n");

	ret = callme0(file, Close);
	printf("(%p)->Close(%p) => %d\n", VOIDPTR(file), VOIDPTR(file), ret);

	ret = callme(drv, Remove, non_exist_filename);
	printf("(%p)->Remove(%p, \"%s\") => %d\n", VOIDPTR(drv), VOIDPTR(drv), non_exist_filename, ret);

	ret = callme0(drv, Closeall);
	printf("(%p)->Closeall(%p) => %d\n", VOIDPTR(drv), VOIDPTR(drv), ret);
}

int main() {
	test_drv((struct IFileDriver *)FileDriver_isoc_ptr);
#ifdef _WIN32
	test_drv((struct IFileDriver *)FileDriver_win32_ptr);
#else
	test_drv((struct IFileDriver *)FileDriver_posix_ptr);
#endif /* _WIN32 */
	return 0;
}

#ifndef MMAPBUFDIO_IFILEDRIVER_H
#define MMAPBUFDIO_IFILEDRIVER_H

/**
 * \file
 * \brief IDriverFile & IFileDriver interfaces declaration
 */

#include "types.h"
#include "error.h"

/**
 *
 */
typedef enum {
    /* file open modes - At least read_mode or write_mode must always be specified. */
    not_open    = 0,      /**<  */

    read_mode   = 1 << 0, /**< If set open the file in read mode */
    write_mode  = 1 << 1, /**< If set open the file in write mode */
	create_mode = 1 << 2  /**< If set create or truncate the file, else fail if the file does not exist. */
} IoMode;

struct IDriverFile;
struct IFileDriver;

/**
 * \struct IDriverFileVtbl
 * \brief IDriverFile interface Vtable.
 */
struct IDriverFileVtbl {
	/**
	 * Closes the file previously created by \ref IFileDriver "IFileDriver"::\ref IFileDriverVtbl::Open() "Open()",
	 * releases the underlying streams, closes memory mappings, and frees all linked resources.
	 *
	 * \param[in] iface IDriverFile interface.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if \p iface is NULL.
	 * \retval -ERR_INVL        if \p io_mode is not a valid mode.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Close)(struct IDriverFile *iface);

	/**
	 * Retrieves the file size in bytes.
	 *
	 * \param[in]  iface    IDriverFile interface.
	 * \param[out] dst_size (*\p dst_size) will be set to the file size.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p dst_size is NULL.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*GetSize)(struct IDriverFile *iface, uint64_t *dst_size);

	/**
	 * Maps a part of the file to memory.
	 *
	 * \param[in]  iface   IDriverFile interface.
	 * \param[in]  off     An offset in the file where the mapping starts.
	 * \param[in]  len     The length of the mapping.
	 * \param[out] map_ptr (*\p map_ptr) will be set to the mapping start address.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p map_ptr is NULL.
	 * \retval -ERR_INVL        if \p len is 0.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Mmap)(struct IDriverFile *iface, uint64_t off, uint64_t len, void **map_ptr);

	/**
	 * Synchronizes a mapped region with the underlying device.
	 *
	 * \param[in] iface   IDriverFile interface.
	 * \param[in] map_ptr A pointer to the mapping region that will be synchronized.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p map_ptr is NULL.
	 * \retval -ERR_INVL        if \p map_ptr does not belong to a mapped region.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Mflush)(struct IDriverFile *iface, void *map_ptr);

	/**
	 * Removes a mapping previously established by \ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Mmap() "Mmap()".
	 *
	 * \param[in] iface  IDriverFile interface.
	 * \param[in] map_ptr A pointer to the region that will be unmapped.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p map_ptr is NULL.
	 * \retval -ERR_INVL        if \p map_ptr does not belong to a mapped region.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Munmap)(struct IDriverFile *iface, void *map_ptr);

	/**
	 * Reads a specified area from the file to memory.
	 *
	 * \param[in]  iface   IDriverFile interface.
	 * \param[in]  off     An offset in the file where the reading will start.
	 * \param[in]  len     The length of the data to read.
	 * \param[out] dst_ptr A pointer to a buffer to store the file data.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p dst_ptr is NULL.
	 * \retval -ERR_INVL        if \p len is 0.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Read)(struct IDriverFile *iface, uint64_t off, uint64_t len, void *dst_ptr);

	/**
	 * Writes a specified area from the file to memory.
	 *
	 * \param[in] iface   IDriverFile interface.
	 * \param[in] off     An offset in the file to which data will be written.
	 * \param[in] len     The length of the data to write.
	 * \param[in] src_ptr A pointer to a buffer that contains the data to write.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p src_ptr is NULL.
	 * \retval -ERR_INVL        if \p len is 0.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IDriverFile implementation
	 *                          for this driver.
	 */
	generic_error (*Write)(struct IDriverFile *iface, uint64_t off, uint64_t len, void *src_ptr);
};

/**
 * \interface IDriverFile
 * \brief Interface to access driver specific functions for a given file.
 */
struct IDriverFile {
	struct IDriverFileVtbl *lpVtbl;        /**< IDriverFile methods          */
	struct IFileDriver *IFileDriver_iface; /**< Parent IFileDriver interface */
};

/**
 * \struct IFileDriverVtbl
 * \brief IFileDriver interface Vtable.
 */
struct IFileDriverVtbl {
	/**
	 * Opens a file and returns it in a new allocated IDriverFile object.
	 *
	 * \param[in]  iface     IFileDriver interface.
	 * \param[in]  file_path Path of the file that need to be opened for I/O.
	 * \param[in]  io_mode   File open mode.
	 * \param[out] file      (*\p file) will be set to a newly allocated instance of
	 *                       IDriverFile for the requested \p file_path and \p io_mode.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface, \p file_path or \p file is NULL.
	 * \retval -ERR_INVL        if \p io_mode is not a valid mode.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IFileDriver implementation
	 *                          for this driver.
	 */
	generic_error (*Open)(struct IFileDriver *iface, const char *file_path, IoMode io_mode, struct IDriverFile **file);

	/**
	 * Calls \ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Close() "Close()"
	 * on each file previously opened by the driver.
	 *
	 * \param[in] iface IFileDriver interface.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if \p iface is NULL.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IFileDriver implementation.
	 *                          for this driver.
	 */
	generic_error (*Closeall)(struct IFileDriver *iface);

	/**
	 * Removes a given file or directory.
	 *
	 * \param[in] iface     IFileDriver interface.
	 * \param[in] file_path Path of the file to remove.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          on success.
	 * \retval -ERR_INVL        if either \p iface or \p file_path is NULL.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IFileDriver implementation
	 *                          for this driver.
	 */
	generic_error (*Remove)(struct IFileDriver *iface, const char *file_path);

	/**
	 * Checks if the driver support file mapping functions
	 * (\ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Mmap() "Mmap()",
	 *  \ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Mflush() "Mflush()",
	 *  \ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Munmap() "Munmap()").
	 *
	 * \param[in] iface IFileDriver interface.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          if file mapping functions are available.
	 * \retval -ERR_INVL        if \p iface is NULL.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IFileDriver implementation
	 *                          for this driver.
	 * \retval -ERR_NOTIMPL     if file mapping functions are not available
	 *                          (each call to one of them will return in -ERR_NOTIMPL).
	 */
	generic_error (*IsMmapSupported)(struct IFileDriver *iface);

	/**
	 * Checks if the driver supports file streaming functions
	 * (\ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Read() "Read()",
	 *  \ref IDriverFile "IDriverFile"::\ref IDriverFileVtbl::Write() "Write()").
	 *
	 * \param[in] iface IFileDriver interface.
	 *
	 * \returns 0 on success, < 0 on any error (negated values from ::generic_error enum).
	 *
	 * \retval SUCCESS          if file streaming functions are available.
	 * \retval -ERR_INVL        if \p iface is NULL.
	 * \retval -ERR_WRONG_IFACE if \p iface is not a valid IFileDriver implementation
	 *                          for this driver.
	 * \retval -ERR_NOTIMPL     if file streaming functions are not available
	 *                          (each call to one of them will result in -ERR_NOTIMPL).
	 */
	generic_error (*IsStreamSupported)(struct IFileDriver *iface);
};

/**
 * \interface IFileDriver
 * \brief Interface to create IDriverFile instance from a specified driver.
 */
struct IFileDriver {
	struct IFileDriverVtbl *lpVtbl; /**< IFileDriver methods */
};

#endif /* MMAPBUFDIO_IFILEDRIVER_H */

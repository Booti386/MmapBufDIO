/**
 * \file
 * \brief Unified interface implementation to use IFileDrivers functionnalities
 */

#include "platform.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "error.h"
#include "FileInterface.h"

#include "FileDriver_isoc.h"
#ifdef _WIN32
#  include "FileDriver_win32.h"
#else
#  include "FileDriver_posix.h"
#endif /* _WIN32 */

#define callme(This, func, ...) This->lpVtbl->func(This, ## __VA_ARGS__)
#define callme0(This, func) This->lpVtbl->func(This)
#define VOIDPTR(ptr) ((void *)ptr)

struct FileInterface {
	struct IFileDriver *drv;
	struct IDriverFile *file;
	IoOpts opts;
	IoMode mode;
};

static const struct IFileDriver *FileDriver_null_ptr = NULL;

static const struct IFileDriver **driver_list[DRIVER_MAX] = {
#ifdef _WIN32
	&FileDriver_null_ptr,
#else
	&FileDriver_posix_ptr,
#endif
#ifdef _WIN32
	&FileDriver_win32_ptr,
#else
	&FileDriver_null_ptr,
#endif
	&FileDriver_isoc_ptr
};

generic_error fiCreate(struct FileInterface **fi, driver_id drvid)
{
	struct FileInterface *new_fi;

	if(!fi || drvid >= DRIVER_MAX)
		return -ERR_INVL;
	if(*driver_list[drvid] == FileDriver_null_ptr)
		return -ERR_NOTIMPL;

	new_fi = malloc(sizeof(*new_fi));
	if(!new_fi)
		return -ERR_NOMEM;

	new_fi->drv = (struct IFileDriver *)*driver_list[drvid];
	new_fi->file = NULL;
	new_fi->mode = not_open;
	*fi = new_fi;
	return SUCCESS;
}

generic_error fiDestroy(struct FileInterface **fi)
{
	struct FileInterface *pfi;
	if(!fi || !*fi)
		return -ERR_INVL;
	pfi = *fi;

	/* ... */

	if(pfi->file)
		callme0(pfi->file, Close);

	free(*fi);
	*fi = NULL;
	return SUCCESS;
}

generic_error fiIsOptsSupported(struct FileInterface *fi, IoOpts opt)
{
	if(opt & io_flag_buffered)
		return -ERR_NOTIMPL;

	if((opt & io_opt_mask) == memory_mapped_io
			&& callme0(fi->drv, IsMmapSupported) < 0)
		return -ERR_NOTIMPL;
	else if((opt & io_opt_mask) == standard_io
			&& callme0(fi->drv, IsStreamSupported) < 0)
		return -ERR_NOTIMPL;
	return SUCCESS;
}

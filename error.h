#ifndef MMAPBUFDIO_ERROR_H
#define MMAPBUFDIO_ERROR_H

/**
 * \file
 * \brief Error kinds listing
 */

/**
 * \enum generic_error
 * \brief Generic error values
 */
typedef enum {
	SUCCESS         = 0,
	ERR_UNK         = 1,
	ERR_NOTIMPL     = 2,
	ERR_INVL        = 3,
	ERR_NOMEM       = 4,
	ERR_WRONG_IFACE = 5,
	ERR_PERMS       = 6,
	ERR_NOSPC       = 7,
	ERR_IO          = 8,
	ERR_EOF         = 9,
	ERR_OVERFLOW    = 10,
	ERR_BUSY        = 11,
	ERR_NOTEXIST    = 12
} generic_error;

#endif /* MMAPBUFDIO_ERROR_H */
